https://github.com/bnb/awesome-hyper

# Setup

1. Windows Store - Search and Install 'Ubuntu' - Start it, open the site it says, and run the command in PowerShell the site says. - Restart PC
2. Download and install [Git](https://git-scm.com/download/win)
3. Download and install [Hyper](https://hyper.is/)
4. Download and install [VcXsrv](https://sourceforge.net/projects/vcxsrv/)
5. Run the windows prepare script
6. Run the Ubuntu script form the subsystem
7. Set the a powerline font!

# Useful stuff

### byobu
Terminal Multiplexer
starts automatically by starting it, pressing F1 and then select the start automatically option

### vim
Text Editor

##### ^Z
Sends the current instance of vim to the background (quickly reopen it with 'fg')

##### :setf [language] 
Sets the recognized language as "language"

example : ":setf cpp"
example : ":setf java"

##### :tabe [file]
Opens a file in a new tab

##### :tabn n
Opens the nth tab

### zsh
Extended Bourne Again Shell

### (fg [ %job_id ])[https://www.computerhope.com/unix/ufg.htm]
job_id : any natural number

Typing fg will resume the most recently suspended or backgrounded job.

example: "fg %1" opens the process to the foreground which was suspended first
example: "fg" opens the process to the foreground which was suspended last

### watch [-n SECONDS] ""
watch -n 1 "ls -ltr"

prints the output of a comman in regular intervals

### Establishing ssh keys
A is the client who wants to connect to B through ssh
Then A creates an rsa key-pair with the command:_
```bash
ssh-keygen -t rsa -b 4096
```
A doesn't forget to secure his new private key:
```bash
chmod -R 0755 ~/.ssh/*
```

He will then get an ~/.ssh/id__rsa and an ~/.ssh/id_rsa.pub files by default
Then A will copy the .pub file over B 
```bash
scp ~/.ssh/id_rsa.pub username@remotehost:~/.ssh/uploaded_rsa.pub
```

and then echo it's contents inside B's ~/.ssh/authorized_keys file as so:
```bash
cat ~/.ssh/uploaded_rsa.pub >> ~/.ssh/authorized_keys
```

At this point, A can connect to B without a password as so:
```bash
ssh -i ~/.ssh/id_rsa username@remotehost
```
