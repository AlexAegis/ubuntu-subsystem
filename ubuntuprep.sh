
sudo bash

apt-get -y update
apt-get -y upgrade

# If you are on aneed shared clipboard on a virtual ubuntu
# sudo apt-get install gcc make perl
# attach oracles VBox-GAs and click Run Software

apt-get -y install git npm vim zsh byobu python-pip curl fonts-powerline

# games
apt-get -y install nsnake greed

# install the preconfigured vim
sh <(curl https://j.mp/spf13-vim3 -L)
# install the preconfigured oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# install powerline
su -c 'pip install git+git://github.com/Lokaltog/powerline'
sudo -H pip install --upgrade pip
# install powerline symbols
wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
sudo mv PowerlineSymbols.otf /usr/share/fonts/
sudo fc-cache -vf
sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/


 
# load powershell for bash on start, either its installed locally or globally
echo "
if [ -f /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh ]; then
    source /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh
fi

if [ -f ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh ]; then
    source ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh
fi
" >> ~/.bashrc

# load powershell for zsh on start, either its installed locally or globally
echo "
if [[ -r /usr/local/lib/python2.7/dist-packages/powerline/bindings/zsh/powerline.zsh ]]; then
    source /usr/local/lib/python2.7/dist-packages/powerline/bindings/zsh/powerline.zsh
fi

if [[ -r ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh ]]; then
    source ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh
fi
" >> ~/.zshrc
# powerline daemon starter script

echo "

powerline_init() {

    is_powerline_session() {
        hash powerline-daemon >/dev/null 2>&1
    }

    if is_powerline_session; then 
        powerline-daemon -q
        export POWERLINE_BASH_CONTINUATION=1
        export POWERLINE_BASH_SELECT=1
    fi
}

# ...

powerline_init 

if [[ -d ~/.local/bin ]]; then
    export PATH=\$PATH:\$HOME/.local/bin
fi
"  | tee -a ~/.bashrc ~/.zshrc

# setting vimrc before local
echo "
let g:airline_powerline_fonts=1" >> ~/.vimrc.before.local

# setting vimrc local

echo "
set expandtab!
let g:neocomplete#enable_at_startup = 1

set rtp+=,\$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/,/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/

\" Always show statusline
set laststatus=2

\" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256" >> ~/.vimrc.local

# change default shell in byobu
echo "set -g default-shell /usr/bin/zsh
set -g default-command /usr/bin/zsh
set-option -g default-terminal \"screen-256color\"
set -g mouse on

" >> ~/.byobu/.tmux.conf

if [[ -r /usr/local/lib/python2.7/dist-packages/powerline/bindings/tmux/powerline.conf ]]; then
    echo "source /usr/local/lib/python2.7/dist-packages/powerline/bindings/tmux/powerline.conf" >> ~/.byobu/.tmux.conf
fi

if [[ -r ~/.local/lib/python2.7/site-packages/powerline/bindings/tmux/powerline.conf ]]; then
    echo "source ~/.local/lib/python2.7/site-packages/powerline/bindings/tmux/powerline.conf" >> ~/.byobu/.tmux.conf
fi

# change default color scheme in byobu
echo "
color_map() {
        case \"\$1\" in
                \"k\") _RET=\"black\" ;;
                \"r\") _RET=\"red\" ;;
                \"g\") _RET=\"green\" ;;
                \"y\") _RET=\"yellow\" ;;
                \"b\") _RET=\"blue\" ;;
                \"m\") _RET=\"magenta\" ;;
                \"c\") _RET=\"cyan\" ;;
                \"w\") _RET=\"white\" ;;
                \"d\") _RET=\"black\" ;;
                \"K\") _RET=\"brightblack\" ;;
                \"R\") _RET=\"brightred\" ;;
                \"G\") _RET=\"brightgreen\" ;;
                \"Y\") _RET=\"brightyellow\" ;;
                \"B\") _RET=\"af0000\" ;;
                \"M\") _RET=\"brightmagenta\" ;;
                \"C\") _RET=\"brightcyan\" ;;
                \"W\") _RET=\"ffff87\" ;;
                *) _RET= ;;
        esac
}" >> ~/.byobu/color.tmux



echo "
set autoindent smartindent noexpandtab tabstop=4 shiftwidth=4
" >> ~/.vimrc.local

